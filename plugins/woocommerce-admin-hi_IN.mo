��    P      �  k         �     �     �  @   �     -     4     B     R     W     c     k     t     �     �  
   �     �     �     �     �     �     �  
   �     �     �  	   �     �     �     �               "     .     7     >     K     ]     s     �     �     �     �     �     �     �     �     �     �     �     	     	     3	     O	     X	     ]	     p	     w	  
   {	     �	     �	  O   �	     �	     �	     �	     �	  
   
     
  
   
     (
     /
  	   J
     T
     i
  %   ~
     �
     �
     �
     �
     �
     �
     �
    
  &        >     T     �     �  ,     	   .     8     O     \  /   o     �     �     �  2   �     �          (     5  0   B  	   s     }     �     �     �  	   �  9   �       >     0   Z     �     �  &   �     �     �       "   "  7   E     }     �  .   �     �     �       %     	   C  #   M     q     �     �     �     �     �     �       )        @     W  �   n  	   >  	   H  /   R  #   �     �     �  "   �     �  G   �     >     U     t  %   �     �     �     �  +     "   .  ,   Q  ,   ~     0   -       &   2   7   #   D   6   
       C            ;   L       H             5   K              P   ,   1             8       E          "   ?              	   O         F   G         !                     B   I   J   /                A       $             '              :   +                   9      >       (         .   %   @   4          3   <   *              M          )   N   =    Add to cart Admin menu nameOrders An e-commerce toolkit that helps you sell anything. Beautifully. Cancel Checkout Page Choose an image City Coupon code Coupons Customer Customer note Date Description Dimensions Display type Docs Edit Email Emails Enter a value First name In stock Item Last name N/A Name No variations added Order Order Notes Order Total Order by Orders Out of stock Page settingCart Page settingCheckout Page settingMy Account Pay Please select some items. Price Product Product categories Products Qty Reviews Save changes Search Select a country&hellip; Settings Settings group labelCheckout Settings tab labelCheckout Shipping Show Specific Countries Status Tax Tax Status Taxable Tel: This is a demo store for testing purposes &mdash; no orders shall be fulfilled. Total Totals Username Variation #%s of %s Variations View View Order Weight WooCommerce Recent Reviews WooThemes default-slugproduct http://woothemes.com http://www.woothemes.com/woocommerce/ out of 5 product slugproduct slugproduct-category slugproduct-tag variation added variations added Project-Id-Version: WooCommerce
Report-Msgid-Bugs-To: https://github.com/woothemes/woocommerce/issues
POT-Creation-Date: 2014-12-16 14:43:08+00:00
PO-Revision-Date: 2014-12-11 17:47+0000
Last-Translator: Claudio Sanches <contato@claudiosmweb.com>
Language-Team: Hindi (India) (http://www.transifex.com/projects/p/woocommerce/language/hi_IN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi_IN
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: grunt-wp-i18n 0.4.9
 टोकरी में डाले ऑर्डर्स आसानी से आपको कुछ भी बेचने दे ऐसा ई-कौमर्स टूलकिट रद्द करें भुगतान एक प्रतिमा चुनें शहर कूपन कोड कूपन ग्राहक ग्राहक को टिप्पणी तारीक विवरण आयाम प्रदर्शन के प्रकार दस्तावेज़ एडिट ईमेल ईमेल एक मूल्य दर्ज करें नाम उपलभ्ध है प्रॉडक्ट कुलनाम N/A नाम भिन्नता जोड़ी नहीं गई ऑर्डर ऑर्डर सम्बंधित टिप्पणी ऑर्डर की कुल क़ीमत क्रम चयन ऑर्डर्स स्टाक में नहीं टोकरी भुगतान मेरा अकाउंट मूल्य प्रदान कृपया कुछ आइटम चुनिए. क़ीमत प्रॉडक्ट उत्पाद श्रेणियों प्रॉडक्ट्स मात्रा समीक्षा परिवर्तन बचाए खोज कोई देश चुनें सेट्टिंग्स भुगतान भुगतान शिप्पिंग देखाए विशिष्ट देश स्थति कर टैक्स की स्थिति कर योग्य फोन नंबर परीक्षण प्रयोजनों के लिए एक डेमो दुकान है - कोई भी ऑर्डर को पूरा नहीं किया जाएगा. कुल कुल उपयोगकर्ता का नाम प्रकार #%s चयन %s प्रकार देखे ऑर्डर्स देखे वज़न वूकोमर्स : हाल की समीक्षाएँ वू थीम्स प्रॉडक्ट्स http://woothemes.com http://www.woothemes.com/woocommerce/ ५ में से प्रॉडक्ट्स प्रॉडक्ट्स प्रॉडक्ट-श्रेणी प्रॉडक्ट-टॅग भिन्नता जोड़ी गई भिन्नता जोड़ी गई 